FROM node:8

RUN  apt-get update -y && \
     apt-get upgrade -y && \
     apt-get dist-upgrade -y && \
     apt-get -y autoremove && \
     apt-get clean

RUN apt-get install -y zip libzmq3 libzmq3-dev

RUN npm install --global npm@6.1.0
RUN npm install --global serverless@1.27.3
RUN npm install --global webpack@4.9.1
RUN npm install --global serverless-pseudo-parameters serverless-dynamodb-autoscaling serverless-stack-output serverless-plugin-tracing serverless-webpack

ENTRYPOINT []